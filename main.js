var MAX_DAYS_PASSED = 300; // The maximum number of days ago a commit is allowed to have been made
                           // before it is rejected. This can catch copy-pasted answers.
var DEBUG_MODE = true;

var log_debug = function () {}; // No logging by default

(function() {
  initVerboseMode();
  initChannel();

  return {
    getGrade: getGrade,
    setState: setState,
    getState: getState
  };
}());

function initVerboseMode() {
  if (DEBUG_MODE) {
    log_debug = console.log;
    log_debug("Entering debug mode.");
  }
}

function initChannel() {
  if (window.parent !== window) {
    channel = Channel.build({
      window: window.parent,
      origin: '*',
      scope: 'JSInput'
    });

    channel.bind('getGrade', getGrade);
    channel.bind('getState', getState);
    channel.bind('setState', setState);
  }
}

function getGrade() {
  var logText = getLogText();
  log_debug('Getting grade with input "' + logText + '"');

  try {
    var logData = parseCommitLines(logText);
  } catch (error) {
    setErrorOutput(error.message);
    return JSON.stringify('incorrect');
  }

  if (!areLogsCorrect(logData)) {
    return JSON.stringify('incorrect');
  }

  return JSON.stringify('correct');
}

function getState() {
  return JSON.stringify({
    text: getLogText(),
    error: getErrorOutput()
  });
}

function setState() {
  var dataStr = (arguments.length === 1) ? arguments[0] : arguments[1];
  var stateData = JSON.parse(dataStr);

  if (stateData && stateData.text) {
    setLogText(stateData.text);
  }
  if (stateData && stateData.error) {
    setRawErrorOutput(stateData.error);
  }
}

// This function calculates whether the answer is correct based on the parsed
// log data. Modify this when using other modules.
function areLogsCorrect(logData) {
  if (!logData || logData.length !== 2) {
    setErrorOutput('There should be 2 commits in the log.');
    return false;
  }

  var commitMsg = logData[0].message.trim().toLowerCase();
  if (!commitMsg.includes('this is a commit')) {
    setErrorOutput('Incorrect commit message.');
    return false;
  }

  clearErrorOutput();
  return true;
}

function setErrorOutput(text) {
  setRawErrorOutput('Incorrect: ' + text);
}

function setRawErrorOutput(text) {
  document.querySelector('.errorOutput').innerText = text;
}

function getErrorOutput() {
  return document.querySelector('.errorOutput').innerText;
}

function clearErrorOutput() {
  document.querySelector('.errorOutput').innerText = '';
}

function parseCommitLines(logText) {
  var textSplit = logText.split(/[\r\n]+/g);
  log_debug('Split input by newline: ' + textSplit);
  var parsedLogs = textSplit.map(parseCommitLine);

  parsedLogs = parsedLogs.filter(function(logLine) {
    return logLine !== null;
  });

  return parsedLogs;
}

// expects log format "%h,%an,%ae,%ad,%s" and ISO date
function parseCommitLine(logLine) {
  log_debug('Parsing line "' + logLine + '"');
  if (!logLine) {
    log_debug('Line is empty. Skipping.');
    return null;
  }

  var errorMsg = 'Line "' + logLine + '" does not match log format.';
  var data = logLine.match(/^([0-9a-fA-F]{7}(?:[0-9a-fA-F]{33})?),(.+),(.+),(.+),(.+)$/);

  if (!data || data.length !== 6) {
    log_debug('ERROR: Line did not match format: "' + data + '"');
    throw new Error(errorMsg);
  }

  for (var i = 1; i < data.length; i++) {
    data[i] = data[i].trim();

    if (!data[i] || data[i].length === 0) {
      log_debug('ERROR: parameter ' + i + ' is missing in "' + data + '"');
      throw new Error(errorMsg);
    }
  }

  var hashInfo = {
    hash: data[1],
    authorName: data[2],
    authorEmail: data[3],
    date: new Date(data[4]),
    message: data[5]
  };

  if (!isValidDate(hashInfo.date)) {
    log_debug('ERROR: date is incorrect.');
    throw new Error(errorMsg);
  }

  return hashInfo;
}

function isValidDate(date) {
  if (!date) {
    log_debug('ERROR: Missing date.');
    return false;
  }

  var millisAgo = Date.now() - date;
  var daysAgo = millisAgo / (1000 * 60 * 60 * 24);
  log_debug(daysAgo + ' days have passed since commit.');
  return daysAgo > 0 && daysAgo < MAX_DAYS_PASSED;
}

function getLogText() {
  var textArea = document.querySelector('#logInput');
  return textArea.value.trim() || '';
}

function setLogText(newText) {
  var textArea = document.querySelector('#logInput');
  textArea.value = newText;
}

